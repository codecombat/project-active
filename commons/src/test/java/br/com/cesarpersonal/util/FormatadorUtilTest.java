package br.com.cesarpersonal.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.vjrcreative.formatters.FormatadorUtil;

@Test
public class FormatadorUtilTest {

	public void removeFormatacaoTelefone() {
		Assert.assertEquals(FormatadorUtil.removeMaskPhone("(61) 9-8290-6582"), "61982906582");
	}
}
