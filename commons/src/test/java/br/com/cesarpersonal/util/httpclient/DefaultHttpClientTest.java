package br.com.cesarpersonal.util.httpclient;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.vjrcreative.httpclient.DefaultHttpClient;
import br.com.vjrcreative.httpclient.Get;

@Test
public class DefaultHttpClientTest {
	
	private DefaultHttpClient client;
	
	public void getRestParamTest() {
		client = new DefaultHttpClient(new Get("http://viacep.com.br/ws/70680500/json/"));
		Assert.assertNotNull(client);
		Assert.assertTrue(StringUtils.isNotBlank(client.executeAndToString()));
	}

}
