package br.com.cesarpersonal.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import br.com.vjrcreative.converters.DateConverterUtil;

@Test
public class DateConverterUtilTest {

	public void formataDataPadraoTest() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, 15);
		Assert.assertEquals(DateConverterUtil.data(cal.getTime()), "15/02/2017");
	}
	
	public void formataDataPadraoStringTest() throws ParseException {
		Date date = DateConverterUtil.data("15/02/2017");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Assert.assertEquals(cal.get(Calendar.YEAR), 2017);
		Assert.assertEquals(cal.get(Calendar.MONTH), 1);
		Assert.assertEquals(cal.get(Calendar.DATE), 15);
	}
	
	public void formataDataPadraoDataIncorretaExceptionTest() throws ParseException {
		Assert.assertNull(DateConverterUtil.data(""));
	}

	public void formataDataPadraoDataIncorreta2ExceptionTest() throws ParseException {
		Assert.assertNull(DateConverterUtil.data("   "));
	}

	public void formataDataPadraoDataIncorreta3ExceptionTest() throws ParseException {
		Assert.assertNull(DateConverterUtil.data("15;02/2017"));
	}
}
