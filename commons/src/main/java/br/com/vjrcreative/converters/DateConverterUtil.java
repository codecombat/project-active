package br.com.vjrcreative.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateConverterUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateConverterUtil.class);
	
	/** The Constant FORMATO_DATA_PADRAO. */
	public static final String FORMATO_DATA_PADRAO = "dd/MM/yyyy";

	/** The Constant FORMATO_DATA_INVERTIDA. */
	public static final String FORMATO_DATA_INVERTIDA = "yyyy-MM-dd";

	/** The Constant FORMATO_HORA_PADRAO. */
	public static final String FORMATO_HORA_PADRAO = "HH:mm";

	/** The Constant FORMATO_DATA_HORA_PADRAO. */
	public static final String FORMATO_DATA_HORA_PADRAO = "dd/MM/yyyy HH:mm";
	
	public static String data(Date data) {
		Objects.requireNonNull(data);
		SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);
		return sdf.format(data);
	}
	
	public static Date data(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			LOGGER.error("Erro ao tentar converter a data " + date, e);
		}
		return null;
	}
}