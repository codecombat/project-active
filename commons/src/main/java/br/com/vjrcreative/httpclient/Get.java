package br.com.vjrcreative.httpclient;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

/**
 *
 *
 */
public class Get implements HttpMethod {

	private HttpGet httpMethod;

	public Get(String url) {
		httpMethod = new HttpGet(url);
	}

	@Override
	public HttpUriRequest getHttpMethod() {
		return httpMethod;
	}
}