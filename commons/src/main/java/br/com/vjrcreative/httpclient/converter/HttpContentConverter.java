package br.com.vjrcreative.httpclient.converter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;

public class HttpContentConverter {
	
	/**
	 * @param response
	 * @return
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 */
	public static String convert(HttpResponse response) throws UnsupportedOperationException, IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

		StringBuffer result = new StringBuffer();
		String line = StringUtils.EMPTY;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		return result.toString();
	}
}
