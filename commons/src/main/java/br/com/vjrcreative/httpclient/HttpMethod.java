package br.com.vjrcreative.httpclient;

import org.apache.http.client.methods.HttpUriRequest;

public interface HttpMethod {

	HttpUriRequest getHttpMethod();

}
