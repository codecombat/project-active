package br.com.vjrcreative.httpclient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vjrcreative.httpclient.converter.HttpContentConverter;

/**
 * 
 *
 */
public abstract class AbstractHttpClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Post.class);
	
	protected HttpClient client = HttpClientBuilder.create().build();

	private HttpMethod method;

	private HttpResponse response;

	public AbstractHttpClient(HttpMethod requestBase) {
		this.method = requestBase;
	}
	
	public HttpResponse execute() throws ClientProtocolException, IOException {
		preExecute();
		
		response = client.execute(method.getHttpMethod());
		
		LOGGER.debug("Response Code " + response.getStatusLine().getStatusCode());
		return response;
	}
	
	/**
	 * 
	 * 
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	protected abstract void preExecute() throws UnsupportedEncodingException, ClientProtocolException, IOException;
	
	public String executeAndToString() {
		try {
			return HttpContentConverter.convert(execute());
		} catch (Exception e) {
			LOGGER.error("Error attempting to convert content to String", e);
		}
		return null;
	}

}