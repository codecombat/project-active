package br.com.vjrcreative.httpclient;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import lombok.Getter;

/**
 * 
 */
public class Post implements HttpMethod {
	
	@Getter
	private List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	
	@Getter
	private HttpPost httpMethod;
	
	public Post(String url) {
		httpMethod = new HttpPost(url);
	}
	
	/**
	 * 
	 * @param paramName
	 * @param value
	 * 
	 * @return
	 */
	public Post addParameter(String paramName, String value) {
		urlParameters.add(new BasicNameValuePair(paramName, value));
		
		return this;
	}
	
//	@Override
//	protected void preExecute() throws ClientProtocolException, IOException {
//		httpMethod.setEntity(new UrlEncodedFormEntity(urlParameters));
//	}
}