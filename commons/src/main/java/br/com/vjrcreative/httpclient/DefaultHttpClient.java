package br.com.vjrcreative.httpclient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;

public class DefaultHttpClient extends AbstractHttpClient {

	public DefaultHttpClient(HttpMethod method) {
		super(method);
	}
	
	@Override
	protected void preExecute() throws UnsupportedEncodingException, ClientProtocolException, IOException {
	}
}