package br.com.vjrcreative.encrypt;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.management.RuntimeErrorException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class EncriptadorUtil {
	
	private static final String UTF_8 = "UTF-8";
	private static final String ALGORITMO = "AES";
	private static final String encryptionKey = "MZygpewJsCpRrfOr";

	public static String MD5(String content) {
		if (StringUtils.isBlank(content)) {
			return null;
		}
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(content.getBytes()));
			return hash.toString(16);
			
		} catch (NoSuchAlgorithmException e) {
			// probably this exception never occurs
			throw new RuntimeErrorException(null, "NoSuchAlgorithmException");
		}
	}
	
	public static String encrypt(String plainText) throws Exception {
		Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
		byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());

		return Base64.encodeBase64String(encryptedBytes);
	}

	public static String decrypt(String encrypted) throws Exception {
		Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
		byte[] plainBytes = cipher.doFinal(Base64.decodeBase64(encrypted));

		return new String(plainBytes);
	}

	private static Cipher getCipher(int cipherMode) throws Exception {
		String encryptionAlgorithm = ALGORITMO;
		SecretKeySpec keySpecification = new SecretKeySpec(encryptionKey.getBytes(UTF_8), encryptionAlgorithm);
		Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
		cipher.init(cipherMode, keySpecification);

		return cipher;
	}
}