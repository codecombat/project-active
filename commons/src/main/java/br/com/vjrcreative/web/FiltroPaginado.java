package br.com.vjrcreative.web;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class FiltroPaginado {

	protected Integer tamanhoPagina = 15;
	protected Integer pagina = 0;
	
}
