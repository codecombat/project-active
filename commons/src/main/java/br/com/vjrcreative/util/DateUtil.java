package br.com.vjrcreative.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import javax.xml.datatype.XMLGregorianCalendar;

public class DateUtil {

    private static final String PARAMENTRO_OBRIGATORIO = "O parâmetro data é obrigatório";
    
    /**
     * Método responsável por verificar se uma determinada data maior ou igual a
     * data atual
     *
     * @param data
     */
    public static Boolean dataMaiorIgualDataAtual(Date data) {
        validaParametroData(data);

        if (dataMaiorDataAtual(data) || dataIgualDataAtual(data)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static Boolean dataMaiorDataAtual(Date data) {
        validaParametroData(data);

        if (new Date().before(data)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static Boolean dataIgualDataAtual(Date data) {
        validaParametroData(data);

        if (data.compareTo(new Date()) == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static Boolean dataInicialMenorFinal(Date dataInicial, Date dataFinal) {
    	validaParametroData(dataInicial);
    	validaParametroData(dataFinal);
    	
    	if (dataInicial.before(dataFinal)) {
    		return Boolean.TRUE;
    	}
    	
    	return Boolean.FALSE;
    }

    public static Boolean dataMenorDataAtual(Date dataInicial) {
    	return dataInicialMenorFinal(dataInicial, new Date());
    }

    public static Boolean dataInicialMenorIgualFinal(Date dataInicial, Date dataFinal) {
        validaParametroData(dataInicial);
        validaParametroData(dataFinal);

        if (dataInicialMenorFinal(dataInicial, dataFinal) || dataInicial.compareTo(dataFinal) == 0) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    /**
     * Verifica se a data passada é menor que a data atual.
     *
     * @param data
     * @return
     */
    public Boolean dataMenorIgualDataAtual(Date data) {
    	  if (!dataMaiorDataAtual(data) || dataIgualDataAtual(data)) {
              return Boolean.TRUE;
          }
          return Boolean.FALSE;
    }

    public static Integer diferencaEmMesesDataAtual(Date date) {
        validaParametroData(date);
        
        return new Periodo(defineInicioHoraDia(date), new Date()).diferencaMeses();
    }

    public static Integer diferencaEmAnosDataAtual(Date date) {
    	validaParametroData(date);
    	
    	return new Periodo(defineInicioHoraDia(date), new Date()).diferencaAnos();
    }

    public static Integer diferencaEmMesesEntreDatas(Date dataInicial, Date dataFinal) {
        validaParametroData(dataInicial);
        validaParametroData(dataFinal);
        
        return new Periodo(dataInicial, dataFinal).diferencaMeses();
    }

    private static void validaParametroData(Date date) {
        if (date == null) {
            throw new IllegalArgumentException(PARAMENTRO_OBRIGATORIO);
        }
    }

    public static Date toDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            throw new IllegalArgumentException(PARAMENTRO_OBRIGATORIO);
        }
        return calendar.toGregorianCalendar().getTime();
    }

    
    /**
     * Define a data para estar no zero hora 
     * 
     * @param calendar
     * @return 
     */
    private static Date defineInicioHoraDia(Date data) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(data);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.AM_PM, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
    }

	public static Boolean dataMenor(Date data) {
		Objects.requireNonNull(data, "Parâmetro não pode ser null");
		// data maior que a data atual
		if (defineInicioHoraDia(data).compareTo(defineInicioHoraDia(new Date())) < 0) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}