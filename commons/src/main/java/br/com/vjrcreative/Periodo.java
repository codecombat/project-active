package br.com.vjrcreative;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import lombok.Data;

@Data
public class Periodo {

	private Date dataInicio;
	private Date dataFim;

	private Periodo periodo;

	public Periodo(Date dataInicio, Date dataFim) {
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
	}
	
	public Periodo() {
	}

	public Integer diferencaMeses() {
		long meses = ChronoUnit.MONTHS.between(converter(dataInicio), converter(dataFim));
		return new Long(meses).intValue() + 1;
	}

	public Integer diferencaAnos() {
		long anos = ChronoUnit.YEARS.between(converter(dataInicio), converter(dataFim));
		return new Long(anos).intValue();
	}
	
	public Integer getMesDataInicio() {
		return converter(dataInicio).getMonthValue();
	}
	
	private LocalDate converter(Date data) {
		return data.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public Integer getMesDataFim() {
		return converter(dataFim).getMonthValue();
	}

	public Integer getAnoDataInicio() {
		return converter(dataInicio).getYear();
	}

	public Integer getAnoDataFim() {
		return converter(dataFim).getYear();
	}
}