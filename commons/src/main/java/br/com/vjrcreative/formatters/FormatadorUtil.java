package br.com.vjrcreative.formatters;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

import org.apache.commons.lang3.StringUtils;

public class FormatadorUtil {
	
	/**
	 * Remove a formata��o da String passada como par�metro. Utilizado para remover formata��o de CPF, por exemplo.
	 * @param stringFormatada. Ex.: 012.102.504.70
	 * @return 01210250470
	 */
	public static String removeMaskCPF(String stringCPFFormatada) {
		return deleteOldStrings(stringCPFFormatada, ".", "-");
	}
	
	/**
	 * Remove a formatação da String passada como parâmetro. Utilizado para remover formatação de CEP, por exemplo.
	 * @param stringFormatada. Ex.: 012.102.504.70
	 * @return 01210250470
	 */
	public static String removeMaskCEP(String stringCEPFormatada) {
		return removeMaskCPF(stringCEPFormatada);
	}
	
	/**
	 * Remove formata��oo de campos telefone/celular, retornando apenas o DDD + n�mero
	 * @param stringMask (83) 3423-8049
	 * @return 8334238049
	 */
	public static String removeMaskPhone(String stringMask) {
		return deleteOldStrings(stringMask, "(", ")", " ", "-", "_");
	}
	
	public static String removeAlturaMask(String stringAltura) {
		return deleteOldStrings(stringAltura, "_", ",");
	}
	
	private static String deleteOldStrings(String stringMask, String ...args) {
		for (String string : args) {
			stringMask = stringMask.replace(string, StringUtils.EMPTY);
		}
		return stringMask;
	}
	
	public static String formataDouble(String valorDouble) {
		return valorDouble.replace(",", ".");
	}
	
	public static String cep(String cepNaoFormatado) {
//		ValidadorUtil.cepValido(cepNaoFormatado);
		return cepNaoFormatado.substring(0, 2) + "." + cepNaoFormatado.substring(2, 5) + "-" + cepNaoFormatado.substring(5);
	}
	
	public static String telefone(String telefone) {
		String phoneMask= "(##) #####-####";
		MaskFormatter maskFormatter;
		try {
			maskFormatter = new MaskFormatter(phoneMask);
			maskFormatter.setValueContainsLiteralCharacters(false);
			return maskFormatter.valueToString(telefone);
		} catch (ParseException e) {
			return telefone;
		}
	}
}