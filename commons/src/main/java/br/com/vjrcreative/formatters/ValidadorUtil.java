package br.com.vjrcreative.formatters;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidadorUtil {
	
	private static Pattern pattern = java.util.regex.Pattern.compile(".+@.+\\.[a-z]+", Pattern.CASE_INSENSITIVE);

	public static Boolean emailIsValid(String value) {
		if ( value == null || value.length() == 0 ) {
			return true;
		}
		Matcher m = pattern.matcher( value );
		return m.matches();
	}
	
	public static void cepValido(String cep) {
		Objects.requireNonNull(cep, "CEP Vazio");
		if (cep.length() != 8) {
			throw new IllegalArgumentException("O tamanho do CEP tem que ser igual a 8. " + cep);
		}
	}
}