package br.com.vjrcreative.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.enums.Uf;
import br.com.vjrcreative.graficos.DadoGrafico;
import br.com.vjrcreative.graficos.DadoGraficoGenero;
import br.com.vjrcreative.repository.ClienteRepository;
import br.com.vjrcreative.service.utils.AbstractService;

@Stateless
public class ClienteService extends AbstractService<Cliente> {

	private static final long serialVersionUID = 6198014469630135956L;
	
	@Inject
	private ClienteRepository clienteRepository;
	
	@PostConstruct
	public void init() {
		super.repository = this.clienteRepository;
	}

	public void gravar(Cliente cliente) {
		if (StringUtils.isNotBlank(cliente.getNome())) {
			
		}
		cliente.setDataCadastro(LocalDateTime.now());
		cliente.getContato().setCliente(cliente);
		cliente.getEndereco().setCliente(cliente);
		cliente.getInformacoesAdicionais().setCliente(cliente);
		if (Objects.isNull(cliente.getId())) {
			repository.persist(cliente);
			return;
		}
		repository.merge(cliente);
	}

	public List<Cliente> consultar(ClienteFiltroPaginado cliente) {
		return ((ClienteRepository)repository).consultar(cliente);
	}
	
	public List<DadoGraficoGenero> gerarRelatorioGenero() {
		return ((ClienteRepository)repository).gerarRelatorioGenero();
	}

	public List<DadoGrafico> gerarRelatorioClientesEstado(Uf uf) {
		return  ((ClienteRepository)repository).gerarRelatorioClientesEstado(uf);
	}
}