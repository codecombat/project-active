package br.com.vjrcreative.service.utils;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.Predicate;

import br.com.vjrcreative.repository.utils.Repository;
import br.com.vjrcreative.util.BaseEntity;

public abstract class AbstractService<T extends BaseEntity> implements Service<T> {

	private static final long serialVersionUID = 1534775716262875367L;

	protected Repository<T> repository;

	public T find(Long id) {
		return repository.find(id);
	}

	public List<T> findAll() {
		return repository.findAll();
	}

	public void persist(T entidade) {
		repository.persist(entidade);
	}

	public T merge(T entidade) {
		return repository.merge(entidade);
	}

	public void remove(T entidade) {
		repository.remove(entidade);
	}

	public Long count(Predicate whereConditions) {
		return count(Arrays.asList(whereConditions));
	}

	public Long count(List<Predicate> whereConditions) {
		return repository.count(whereConditions);
	}

	public Long count() {
		return repository.count();
	}
}