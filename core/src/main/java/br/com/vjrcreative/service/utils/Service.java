package br.com.vjrcreative.service.utils;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.Predicate;

public interface Service<T> extends Serializable {
	
	public T find(Long id);
	public List<T> findAll();
	public void persist(T entidade);
	public T merge(T entidade);
	public void remove(T entidade);
	public Long count(Predicate whereConditions);
	public Long count();
}
