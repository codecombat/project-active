package br.com.vjrcreative.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.web.FiltroPaginado;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteFiltroPaginado extends FiltroPaginado {
	
	private Cliente cliente;
	
	private List<Predicate> condicoes = new ArrayList<Predicate>();
	
	public ClienteFiltroPaginado(Cliente cliente, Integer tamanhoPagina, Integer pagina) {
		super(tamanhoPagina, pagina);
		this.cliente = cliente;
	}

}
