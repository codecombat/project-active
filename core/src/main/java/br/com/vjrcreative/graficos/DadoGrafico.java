package br.com.vjrcreative.graficos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DadoGrafico {
	
	private String label;
	private Long valor;
}
