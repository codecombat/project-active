package br.com.vjrcreative.graficos;

import br.com.vjrcreative.enums.Sexo;

public class DadoGraficoGenero extends DadoGrafico {

	public DadoGraficoGenero(Sexo sexo, Long valor) {
		super(sexo.getDescricao(), valor);
	}
}