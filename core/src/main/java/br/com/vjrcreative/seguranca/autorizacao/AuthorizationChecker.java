package br.com.vjrcreative.seguranca.autorizacao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.picketlink.Identity;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;

/**
 * This is a utility bean that may be used by the view layer to determine
 * whether the current user has specific privileges.
 * 
 * @author Shane Bryzak
 *
 */
@Named
@Stateless
public class AuthorizationChecker {

	@Inject
	private Identity identity;

	@Inject
	private IdentityManager identityManager;

	@Inject
	private RelationshipManager relationshipManager;

	public boolean hasApplicationRole(String roleName) {
		Role role = BasicModel.getRole(this.identityManager, roleName);
		return BasicModel.hasRole(this.relationshipManager, this.identity.getAccount(), role);
	}

	public boolean isMember(String groupName) {
		Group group = BasicModel.getGroup(this.identityManager, groupName);
		return BasicModel.isMember(this.relationshipManager, this.identity.getAccount(), group);
	}

	public boolean hasGroupRole(String roleName, String groupName) {
		Group group = BasicModel.getGroup(this.identityManager, groupName);
		Role role = BasicModel.getRole(this.identityManager, roleName);
		return BasicModel.hasGroupRole(this.relationshipManager, this.identity.getAccount(), role, group);
	}
}