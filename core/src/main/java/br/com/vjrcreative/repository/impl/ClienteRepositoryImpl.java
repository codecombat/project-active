package br.com.vjrcreative.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.dominio.Endereco;
import br.com.vjrcreative.enums.Sexo;
import br.com.vjrcreative.enums.Uf;
import br.com.vjrcreative.graficos.DadoGrafico;
import br.com.vjrcreative.graficos.DadoGraficoGenero;
import br.com.vjrcreative.repository.AbstractRepository;
import br.com.vjrcreative.repository.ClienteRepository;
import br.com.vjrcreative.service.ClienteFiltroPaginado;

@Dependent
public class ClienteRepositoryImpl extends AbstractRepository<Cliente> implements ClienteRepository {

	protected ClienteRepositoryImpl() {
		super(Cliente.class);
	}

	@Override
	public void gravar(Cliente cliente) {
		if (Objects.isNull(cliente.getId())) {
			getEm().persist(cliente);
			return;
		}
		getEm().merge(cliente);
	}
	
	@Override
	public List<Cliente> consultar(ClienteFiltroPaginado filtro) {
		Cliente cliente = filtro.getCliente();
		CriteriaBuilder cb = getEm().getCriteriaBuilder();
		CriteriaQuery<Cliente> query = cb.createQuery(Cliente.class);
		Root<Cliente> from = query.from(Cliente.class);
		
		query.select(from);
		
		List<Predicate> condicoes = new ArrayList<Predicate>();
		filtro.setCondicoes(condicoes);
		
		String nome = cliente.getNome();
		if (StringUtils.isNotBlank(nome)) {
			condicoes.add(cb.like(cb.lower(from.get("nome")), "%" + cliente.getNome().toLowerCase() + "%"));
		}
		
		Sexo sexo = cliente.getSexo();
		if (Objects.nonNull(sexo)) {
			condicoes.add(cb.equal(from.get("sexo"), sexo));
		}
		
		String logradouro = cliente.getEndereco().getLogradouro();
		if (StringUtils.isNotBlank(logradouro)) {
			condicoes.add(cb.like(cb.lower(from.get("endereco").get("logradouro")), "%" + logradouro.toLowerCase() + "%"));
		}
		
		Uf uf = cliente.getEndereco().getUf();
		if (Objects.nonNull(uf)) {
			condicoes.add(cb.equal(from.get("endereco").get("uf"), uf));
		}
		String localidade = cliente.getEndereco().getLocalidade();
		if (StringUtils.isNotBlank(localidade)) {
			condicoes.add(cb.like(cb.lower(from.get("endereco").get("localidade")),  "%"  + localidade.toLowerCase() +  "%"));
		}
		String bairro = cliente.getEndereco().getBairro();
		if (StringUtils.isNotBlank(bairro)) {
			condicoes.add(cb.like(cb.lower(from.get("endereco").get("bairro")), "%" + bairro.toLowerCase() + "%"));
		}
		
		query.where(condicoes.toArray(new Predicate[condicoes.size()])).orderBy(cb.asc(from.get("nome")));
		TypedQuery<Cliente> createQuery = getEm().createQuery(query);
		if (Objects.nonNull(filtro.getPagina())) {
			createQuery.setFirstResult(filtro.getPagina());
		}
		if (Objects.nonNull(filtro.getTamanhoPagina())) {
			createQuery.setMaxResults(filtro.getTamanhoPagina());
		}
		return createQuery.getResultList();
	}

	@Override
	public List<Cliente> consultar(Cliente cliente) {
		return consultar(new ClienteFiltroPaginado(cliente, null, null));
	}

	@Override
	public List<DadoGraficoGenero> gerarRelatorioGenero() {
		CriteriaBuilder cb = getCriteriaBuilder();
		CriteriaQuery<DadoGraficoGenero> query = cb.createQuery(DadoGraficoGenero.class);
		Root<Cliente> from = query.from(Cliente.class);
		query.multiselect(from.get("sexo"), cb.count(from));
		query.groupBy(from.get("sexo"));

		return getEm().createQuery(query).getResultList();
	}

	@Override
	public List<DadoGrafico> gerarRelatorioClientesEstado(Uf uf) {
		CriteriaBuilder cb = getCriteriaBuilder();
		CriteriaQuery<DadoGrafico> query = cb.createQuery(DadoGrafico.class);
		Root<Endereco> from = query.from(Endereco.class);
		Expression<Long> count = cb.count(from);
		query.multiselect(from.get("bairro"), count);
		query.groupBy(from.get("bairro"));
		query.orderBy(cb.desc(count));
		query.where(cb.equal(from.get("uf"), uf));

		return getEm().createQuery(query).setMaxResults(14).getResultList();
	}

	@Override
	protected EntityManager getEm() {
		// TODO Auto-generated method stub
		return null;
	}
}
