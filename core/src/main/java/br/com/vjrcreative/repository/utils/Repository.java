package br.com.vjrcreative.repository.utils;

import java.util.List;

import javax.persistence.criteria.Predicate;

public interface Repository<T> {
	
	public T find(Long id);
	public List<T> findAll();
	public void persist(T entidade);
	public T merge(T entidade);
	public void remove(T entidade);
	public Long count(Predicate whereConditions);
	public Long count(List<Predicate> whereConditions);
	public Long count();

}
