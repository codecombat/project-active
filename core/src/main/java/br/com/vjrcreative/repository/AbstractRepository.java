package br.com.vjrcreative.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vjrcreative.repository.utils.Repository;
import br.com.vjrcreative.util.BaseEntity;

public abstract class AbstractRepository<T extends BaseEntity> implements Repository<T> {

	private static final String PARAM_OBRIGATORIO = "Parâmetro obrigatório";
	private Logger LOG = LoggerFactory.getLogger(getClass());

	protected Class<T> type;
	
//	@Getter
//	@PersistenceContext
//    protected EntityManager em;
	
	protected abstract EntityManager getEm();
	
	public AbstractRepository() {
	}
	
	protected AbstractRepository(Class<T> type) {
		Objects.requireNonNull(type, PARAM_OBRIGATORIO);
		this.type = type;
	}
	
	@Transactional
	public T find(Long id) {
		Objects.requireNonNull(id, "Parâmetro Id obrigatório");
		return (T) getEm().find(type, id);
	}
	
	@Transactional
	public List<T> findAll() {
		CriteriaQuery<T> q = createQueryFrom();
		return createQuery(q).getResultList();
	}
	
	@Transactional
	public void persist(T entidade) {
		Objects.requireNonNull(entidade, PARAM_OBRIGATORIO);
		getEm().persist(entidade);
	}
	
	@Transactional
	public T merge(T entidade) {
		Objects.requireNonNull(entidade, PARAM_OBRIGATORIO);
		return getEm().merge(entidade);
	}
	
	@Transactional
	public void remove(T entidade) {
		Objects.requireNonNull(entidade, PARAM_OBRIGATORIO);
		getEm().remove(merge(entidade));
	}

	/**
	 * Return number of elements returned by query. If <code>whereConditions</code>, 
	 * is null is the same Select count(*) From T.class
	 * 
	 * @param whereConditions
	 * 
	 * @return
	 */
	@Transactional
	public Long count(Predicate whereConditions) {
		return count(Arrays.asList(whereConditions));
	}
	
	/**
	 * Return number of elements returned by query. If <code>whereConditions</code>, 
	 * is null is the same Select count(*) From T.class
	 * 
	 * @param whereConditions
	 * 
	 * @return
	 */
	@Transactional
	public Long count(List<Predicate> whereConditions) {
		CriteriaQuery<Long> select = createQueryForCount();
		if (whereConditions != null) {
			select.where(whereConditions.toArray(new Predicate[whereConditions.size()]));
		}
		return getEm().createQuery(select).getSingleResult();
	}

	protected CriteriaBuilder getCriteriaBuilder() {
		CriteriaBuilder cb = getEm().getCriteriaBuilder();
		return cb;
	}
	
	protected CriteriaQuery<T> createQuery() {
		return getCriteriaBuilder().createQuery(type);
	}

	protected TypedQuery<T> createQuery(CriteriaQuery<T> query) {
		return getEm().createQuery(query);
	}	
	
	@Transactional
	public Long count() {
		return getEm().createQuery(createQueryForCount()).getSingleResult();
	}
	
	private CriteriaQuery<Long> createQueryForCount() {
		CriteriaQuery<Long> query = createQueryForId();
		Root<T> from = query.from(type);
		CriteriaQuery<Long> select = query.select(getCriteriaBuilder().count(from));
		return select;
	}

	private CriteriaQuery<Long> createQueryForId() {
		return getCriteriaBuilder().createQuery(Long.class);
	}
	
	protected CriteriaQuery<T> createQueryFrom() {
		CriteriaQuery<T> q = createQuery();
		return q.select(q.from(type));
	}
	
	protected T singleResultReturnNullNoResultCase(CriteriaQuery<T> query) {
		try {
			return createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			LOG.info("Resultado Não encontrado. Retornando Null.", e);
			return null;
		}
	}
}
