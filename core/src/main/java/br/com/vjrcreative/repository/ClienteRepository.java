package br.com.vjrcreative.repository;

import java.util.List;

import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.enums.Uf;
import br.com.vjrcreative.graficos.DadoGrafico;
import br.com.vjrcreative.graficos.DadoGraficoGenero;
import br.com.vjrcreative.repository.utils.Repository;
import br.com.vjrcreative.service.ClienteFiltroPaginado;

public interface ClienteRepository extends Repository<Cliente> {
	
	public void gravar(Cliente cliente);

	public List<Cliente> consultar(Cliente cliente);

	/**
	 * Gera o relatório do clientes pelo sexo
	 * @return
	 */
	public List<DadoGraficoGenero> gerarRelatorioGenero();

	/**
	 * Gera o relatório de clientes por estado
	 * @return
	 */
	public List<DadoGrafico> gerarRelatorioClientesEstado(Uf uf);

	/**
	 * Método que faz consulta paginada na tabela de clientes
	 * 
	 * @param cliente
	 * @return
	 */
	public List<Cliente> consultar(ClienteFiltroPaginado cliente);
}
