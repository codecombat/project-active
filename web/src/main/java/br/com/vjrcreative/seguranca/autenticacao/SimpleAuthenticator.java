package br.com.vjrcreative.seguranca.autenticacao;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.picketlink.annotations.PicketLink;
import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.model.basic.User;

@PicketLink
public class SimpleAuthenticator extends BaseAuthenticator {

	private static final String ADMIN = "admin";

	@Inject
	private DefaultLoginCredentials credentials;
	
	@Inject
	private IdentityManager identityManager;
	
	@Inject
	private FacesContext context;

	@Override
	public void authenticate() {
		if (ADMIN.equals(credentials.getUserId()) && ADMIN.equals(credentials.getPassword())) {
			setStatus(AuthenticationStatus.SUCCESS);
			User usuarioLogado = new User(ADMIN);
			usuarioLogado.setFirstName("César");
			usuarioLogado.setLastName("César");
			usuarioLogado.setEmail("cesar@cesarpersonal.com.br");
			setAccount(usuarioLogado);
			
		} else {
			setStatus(AuthenticationStatus.FAILURE);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensagem: ", "Usuário ou senha inválidos."));
		}

	}

}
