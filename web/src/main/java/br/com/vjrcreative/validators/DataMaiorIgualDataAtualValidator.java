package br.com.vjrcreative.validators;

import java.util.Date;
import java.util.Objects;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import br.com.vjrcreative.util.DateUtil;

@FacesValidator("dataMaiorIgualDataAtualValidator")
public class DataMaiorIgualDataAtualValidator implements Validator {

	private static final String DATA_MENSAGEM = "Data tem que ser maior que data atual.";

	@Override
	public void validate(FacesContext context, UIComponent component, Object dataParametro) throws ValidatorException {
		if (Objects.nonNull(dataParametro) && !DateUtil.dataMenorDataAtual((Date) dataParametro)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, DATA_MENSAGEM, DATA_MENSAGEM);
			throw new ValidatorException(msg);
		}
	}

}
