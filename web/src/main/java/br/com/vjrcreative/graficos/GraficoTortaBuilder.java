package br.com.vjrcreative.graficos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GraficoTortaBuilder {
	
	private List<DadoGrafico> dados = new ArrayList<DadoGrafico>();
	private String titulo = StringUtils.EMPTY;
	
	public GraficoTortaBuilder(List<DadoGraficoGenero> list) {
		this.dados = list.stream().map(d -> new DadoGrafico(d.getLabel(), d.getValor())).collect(Collectors.toList());
	}

	public GraficoTortaBuilder lista(List<DadoGrafico> lista) {
		this.dados = lista;
		return this;
	}
	
	public void adicionar(DadoGraficoGenero dado) {
		dados.add(dado);
	}
	
	public PieChartModel gerar() {
		PieChartModel pie = new PieChartModel();
		pie.setLegendPosition("h");
		pie.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
		pie.setShowDataLabels(Boolean.FALSE);
		if (StringUtils.isNotBlank(titulo)) {
			pie.setTitle(titulo);
		}
		if (dados.isEmpty()) {
			pie.set("Não existem dados para gerar o gráfico", 0);
		} else {
			dados.forEach(dado -> pie.set(dado.getLabel(), dado.getValor()));
		}
		return pie;
	}
	
	public GraficoTortaBuilder titulo(String titulo) {
		this.titulo = titulo;
		return this;
	}
}