package br.com.vjrcreative.converters;

import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.formatters.FormatadorUtil;

@FacesConverter("telefoneConverter")
public class TelefoneConverter implements Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String telefoneFormatado) {
		return FormatadorUtil.removeMaskPhone(telefoneFormatado);
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object telefoneSemFormatacao) {
		if (Objects.nonNull(telefoneSemFormatacao) && StringUtils.isNotBlank(telefoneSemFormatacao.toString())) {
			return FormatadorUtil.telefone(telefoneSemFormatacao.toString());
		}
		return null;
	}

}
