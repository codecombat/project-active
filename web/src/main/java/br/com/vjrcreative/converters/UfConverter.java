package br.com.vjrcreative.converters;

import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.enums.Uf;

@FacesConverter("ufConverter")
public class UfConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String sigla) {
		if (StringUtils.isNotBlank(sigla) && !"Selecione".equals(sigla)) {
			return Uf.get(sigla);
		}
		return StringUtils.EMPTY;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object uf) {
		if (Objects.nonNull(uf) && StringUtils.isNotBlank((uf.toString()))) {
			return uf.toString();
		}
		return StringUtils.EMPTY;
	}
}