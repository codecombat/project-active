package br.com.vjrcreative.converters;

import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.enums.Sexo;

@FacesConverter("sexoConverter")
public class SexoConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String sexoCodigo) {
		if (StringUtils.isNotBlank(sexoCodigo) && !"Selecione".equals(sexoCodigo)) {
			return Sexo.get(sexoCodigo);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object sexo) {
		if (Objects.nonNull(sexo) && StringUtils.isNotBlank(sexo.toString())) {
			if (sexo instanceof Sexo) {
				return ((Sexo) sexo).getCodigo();
			}
			return Sexo.get(sexo.toString()).getCodigo();
		}
		return null;
	}
}