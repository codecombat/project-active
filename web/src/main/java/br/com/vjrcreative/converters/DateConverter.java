package br.com.vjrcreative.converters;

import java.util.Date;
import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

@FacesConverter("dateConverter")
public class DateConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String data) {
		if (StringUtils.isNotBlank(data)) {
			return DateConverterUtil.data(data);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object data) {
		if (Objects.nonNull(data) && StringUtils.isNotBlank(data.toString())) {
			return DateConverterUtil.data((Date) data);
		}
		return null;
	}

}
