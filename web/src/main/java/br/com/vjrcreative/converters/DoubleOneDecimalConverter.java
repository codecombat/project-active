package br.com.vjrcreative.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

@FacesConverter("double1DecimalConverter")
public class DoubleOneDecimalConverter extends DoubleConverter {
	
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object doubleObject) {
		String valor = super.getAsString(context, component, doubleObject).replace(",", ".");
		return BigDecimal.valueOf(Double.valueOf(valor)).setScale(1, RoundingMode.HALF_UP).toString().replace(".", ",");
	}
	
}
