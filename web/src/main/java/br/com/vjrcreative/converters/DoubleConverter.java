package br.com.vjrcreative.converters;

import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.formatters.FormatadorUtil;

@FacesConverter("doubleConverter")
public class DoubleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String valorDouble) {
		if (StringUtils.isNotBlank(valorDouble)) {
			return Double.valueOf(FormatadorUtil.formataDouble(valorDouble));
		}
		return null;
	}
	
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object doubleObject) {
		if (StringUtils.isNotBlank(doubleObject.toString()) && Objects.nonNull(doubleObject)) {
			return doubleObject.toString().replace(".", ",");
		}
		return null;
	}}
