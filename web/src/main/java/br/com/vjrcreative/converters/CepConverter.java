package br.com.vjrcreative.converters;

import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.formatters.FormatadorUtil;

@FacesConverter("cepConverter")
public class CepConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String cep) {
		if (StringUtils.isNotBlank(cep)) {
			return cep.replace("-", "").replace(".", "");
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object cepObject) {
		if (Objects.nonNull(cepObject)) {
			return FormatadorUtil.cep(cepObject.toString());
		}
		return null;
	}

}
