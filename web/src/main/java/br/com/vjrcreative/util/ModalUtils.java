package br.com.vjrcreative.util;

import java.util.HashMap;
import java.util.Map;

import org.primefaces.context.RequestContext;

public class ModalUtils {
	
	private String modalName;
	
	public ModalUtils(String modalName) {
		this.modalName = modalName;
	}

	public void confirmacaoModal() {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 640);
        options.put("height", 340);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");
         
        RequestContext.getCurrentInstance().openDialog(modalName, options, null);
    }
	
	public void fecharModal() {
		RequestContext.getCurrentInstance().closeDialog(modalName);
	}
}