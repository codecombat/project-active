package br.com.vjrcreative.controllers.datamodels;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.service.ClienteFiltroPaginado;
import br.com.vjrcreative.service.ClienteService;
import lombok.Getter;
import lombok.Setter;

@ViewAccessScoped
@Named("clienteConsultaLazyDataModel")
public class ClienteConsultaLazyDataModel extends LazyDataModel<Cliente> {

	private static final long serialVersionUID = -7859932536667760319L;
	
	@EJB
	private ClienteService service;
	
	@Getter
	@Setter
	private Cliente filtro = new Cliente();
	
	@Override
	public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		ClienteFiltroPaginado cliente = new ClienteFiltroPaginado(filtro, pageSize, first);
		List<Cliente> resultado = service.consultar(cliente);
		setRowCount(service.count(cliente.getCondicoes()).intValue());
		return resultado;
	}

}
