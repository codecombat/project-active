package br.com.vjrcreative.controllers.utils;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

public abstract class BaseController implements Serializable {
	
	@Inject
	protected FacesContext context;

	private static final long serialVersionUID = -5375301672511957318L;
	
	public boolean showMessageInfo() {
		return FacesContext.getCurrentInstance().getMessageList()
				.stream().map( f -> f.getSeverity().equals(FacesMessage.SEVERITY_INFO)).findAny().orElse(Boolean.FALSE);
	}
	
	protected void addMessage(String message) {
		addMessage(FacesMessage.SEVERITY_INFO, message);
	}

	protected void addAlertMessage(String message) {
		addMessage(FacesMessage.SEVERITY_WARN, message);
	}
	protected void addErrorMessage(String message) {
		addMessage(FacesMessage.SEVERITY_ERROR, message);
	}
	protected void addFatalMessage(String message) {
		addMessage(FacesMessage.SEVERITY_FATAL, message);
	}

	private void addMessage(Severity severity, String message) {
		context.addMessage(null, new FacesMessage(severity, "Mensagem: ", message));
	}
}