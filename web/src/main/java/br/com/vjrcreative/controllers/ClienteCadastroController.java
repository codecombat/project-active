package br.com.vjrcreative.controllers;

import java.util.Objects;

import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;

import br.com.vjrcreative.controllers.utils.AbstractClienteController;
import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.dominio.Endereco;

@Named("clienteCadastroController")
@ViewAccessScoped
public class ClienteCadastroController extends AbstractClienteController {
	
	private static final long serialVersionUID = -1708111169055759465L;
	
	public void buscaCep() {
		String cep = getCliente().getEndereco().getCep();
		Endereco endereco = enderecoController.buscaCep(cep).orElse(getCliente().getEndereco());
		endereco.setCliente(getCliente());
		getCliente().setEndereco(endereco);
	}
	
	public String editar(Cliente clienteSelecionado) {
		setCliente(clienteSelecionado);
		return CLIENTE_CADASTRO;
	}
	
	public String gravar() {
		clienteService.gravar(getCliente());
		adicionaMensagemConfirmacao();
		cliente = new Cliente();
		return CLIENTE_CONSULTA;
	}
	
	private void adicionaMensagemConfirmacao() {
		if (Objects.isNull(getCliente().getId())) {
			addMessage("Cliente cadastrado com sucesso");
		} else {
			addMessage("Cliente alterado com sucesso");
		}
	}
	
	public String labelBotao() {
		return Objects.isNull(cliente.getId()) ? "Gravar": "Alterar";
	}
}