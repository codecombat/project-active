package br.com.vjrcreative.controllers;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;
import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.credential.DefaultLoginCredentials;

import br.com.vjrcreative.controllers.utils.BaseController;
import br.com.vjrcreative.dominio.Usuario;
import lombok.Getter;
import lombok.Setter;

@Named("loginController")
@ViewAccessScoped
public class LoginController extends BaseController {

	private static final String TELA_DASHBOARD = "/dashboard.xhtml?";
	private static final long serialVersionUID = 8073405187982303942L;
	public static final String TELA_LOGIN = "/login.xhtml";
	
	@Inject
	private Identity identity;
	
	@Getter
	@Inject
    private DefaultLoginCredentials loginCredentials;

	@Getter
	@Setter
	private Usuario usuario = new Usuario();
	
	public String logar() {
		AuthenticationResult result = identity.login();
        if (AuthenticationResult.FAILED.equals(result)) {
            return StringUtils.EMPTY;
        }
		return TELA_DASHBOARD + "?faces-redirect=true";
	}
	
	public String logout() {
        this.identity.logout();
        return TELA_LOGIN + "?faces-redirect=true";
    }
}