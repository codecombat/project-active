package br.com.vjrcreative.controllers;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;

import br.com.vjrcreative.controllers.datamodels.ClienteConsultaLazyDataModel;
import br.com.vjrcreative.controllers.utils.AbstractClienteController;
import lombok.Getter;
import lombok.Setter;

@Named("clienteConsultarController")
@ViewAccessScoped
public class ClienteConsultarController extends AbstractClienteController {
	
	private static final long serialVersionUID = 2142270088480217763L;
	
	@Getter
	@Setter
	@Inject
	private ClienteConsultaLazyDataModel model;
	
}