package br.com.vjrcreative.controllers;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;
import org.picketlink.credential.DefaultLoginCredentials;
import org.primefaces.model.chart.PieChartModel;

import br.com.vjrcreative.controllers.utils.BaseController;
import br.com.vjrcreative.enums.Uf;
import br.com.vjrcreative.graficos.GraficoTortaBuilder;
import br.com.vjrcreative.service.ClienteService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@ViewAccessScoped
@Named("dashboardController")
public class DashboardController extends BaseController {

	private static final long serialVersionUID = 8279717401955368895L;
	
	@Getter(value = AccessLevel.PUBLIC)
	protected List<Uf> estados = Uf.valuesAsList();
	
	@Getter
	@Inject 
	private DefaultLoginCredentials credentials;
	
	@Getter
	@Setter
	private Uf estadoSelecionado = Uf.DF;
	
	@EJB
	@Getter
	private ClienteService clienteService;
	
	public String quantidadeClientes() {
		return clienteService.count().toString();
	}
	
	public PieChartModel gerarRelatorioGenero() {
		return new GraficoTortaBuilder(clienteService.gerarRelatorioGenero()).gerar();
	}
	
	public PieChartModel gerarRelatorioClientesEstado() {
		return new GraficoTortaBuilder().lista(clienteService.gerarRelatorioClientesEstado(estadoSelecionado)).gerar();
	}
}