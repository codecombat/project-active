package br.com.vjrcreative.controllers;

import java.util.Objects;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.vjrcreative.controllers.utils.BaseController;
import br.com.vjrcreative.dominio.Endereco;
import br.com.vjrcreative.formatters.FormatadorUtil;
import br.com.vjrcreative.formatters.ValidadorUtil;
import br.com.vjrcreative.httpclient.DefaultHttpClient;
import br.com.vjrcreative.httpclient.Get;

@ManagedBean
@RequestScoped
public class EnderecoController extends BaseController {

	private static final long serialVersionUID = -7906982444664917653L;

	public Optional<Endereco> buscaCep(String cep) {
		if (StringUtils.isBlank(cep)) {
			return Optional.empty();
		}
		Objects.requireNonNull(cep, "Atributo CEP Obrigatório");
		cep = FormatadorUtil.removeMaskCEP(cep);
		ValidadorUtil.cepValido(cep);
		DefaultHttpClient client = new DefaultHttpClient(new Get("http://viacep.com.br/ws/" + cep + "/json/"));
		Gson json = new GsonBuilder().create();
		Endereco enderecoEncontrado = json.fromJson(client.executeAndToString(), Endereco.class);
		return Optional.of(enderecoEncontrado);
	}
	
}
