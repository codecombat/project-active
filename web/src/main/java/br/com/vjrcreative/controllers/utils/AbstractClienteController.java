package br.com.vjrcreative.controllers.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.inject.Inject;

import br.com.vjrcreative.controllers.EnderecoController;
import br.com.vjrcreative.dominio.Cliente;
import br.com.vjrcreative.enums.Sexo;
import br.com.vjrcreative.enums.Uf;
import br.com.vjrcreative.service.ClienteService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public abstract class AbstractClienteController extends BaseController {
	
	private static final long serialVersionUID = 8463033833307252855L;

	protected static final String CLIENTE_CADASTRO = "cliente-cadastro?faces-redirect=true";
	protected static final String CLIENTE_CONSULTA = "cliente-consulta?faces-redirect=true";
	
	@Setter(value = AccessLevel.PUBLIC)
	protected Cliente cliente = new Cliente();
	
	@Getter(value = AccessLevel.PUBLIC)
	protected List<Sexo> genero = Sexo.valuesAsList();
	
	@Getter(value = AccessLevel.PUBLIC)
	protected List<Uf> estados = Uf.valuesAsList();
	
	@Getter(value = AccessLevel.PUBLIC)
	protected List<Cliente> clientes = new ArrayList<Cliente>();
	
	@EJB
	@Getter(value = AccessLevel.PUBLIC)
	protected ClienteService clienteService;
	
	@Inject
	@Getter(value = AccessLevel.PUBLIC)
	protected EnderecoController enderecoController;

	public Cliente getCliente() {
		return Objects.isNull(cliente) ? new Cliente() : cliente;
	}
	
	public void init() {
	}

}
