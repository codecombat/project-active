package br.com.vjrcreative.dominio;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.vjrcreative.enums.Sexo;
import br.com.vjrcreative.enums.converters.DateToLocalDateTimeDataBaseConverter;
import br.com.vjrcreative.enums.converters.SexoDataBaseConverter;
import br.com.vjrcreative.util.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(exclude = {"contato", "endereco", "informacoesAdicionais"})
@EqualsAndHashCode(callSuper = false, exclude = {"contato", "endereco", "informacoesAdicionais"})
@Entity
@Table(name = "CLIENTE", schema = "public")
public class Cliente extends BaseEntity {

	private static final long serialVersionUID = -8516211897446143490L;
	private static final String CLIENTE_SEQ = "CLIENTE_SEQ";
	
	@Id
	@NotNull
	@SequenceGenerator(name = CLIENTE_SEQ, sequenceName = CLIENTE_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CLIENTE_SEQ)
	@Column(name = "ID", unique = true, nullable = false)
	protected Long id;
	
	@NotNull
	@Length(max = 60)
	@Column(name = "NOME", nullable = false, length = 60)
	protected String nome;
	
	@Column(name = "SEXO", nullable = false, length = 1)
	@Convert(converter = SexoDataBaseConverter.class)
	protected Sexo sexo;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_NASCIMENTO", nullable = false)
	protected Date dataNascimento;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_CADASTRO", nullable = false)
	@Convert(converter = DateToLocalDateTimeDataBaseConverter.class)
	protected LocalDateTime dataCadastro;
	
	@OneToOne(mappedBy = "cliente", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	protected Contato contato = new Contato();

	@OneToOne(mappedBy = "cliente", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	protected Endereco endereco = new Endereco();
	
	@OneToOne(mappedBy = "cliente", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	protected InformacoesAdicionais informacoesAdicionais = new InformacoesAdicionais();

}