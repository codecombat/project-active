package br.com.vjrcreative.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"cliente"})
@Entity
@Table(name = "CONTATO", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "ID_CLIENTE"))
public class Contato implements java.io.Serializable {

	private static final long serialVersionUID = -5507115598665919736L;
	private static final String CONTATO_SEQ = "CONTATO_SEQ";
	
	@Id
	@NotNull
	@SequenceGenerator(name = CONTATO_SEQ, sequenceName = CONTATO_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CONTATO_SEQ)
	@Column(name = "ID", unique = true, nullable = false)
	protected Long id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE", nullable = false)
	protected Cliente cliente;
	
	@Length(max = 11)
	@Column(name = "TELEFONE", length = 11)
	protected String telefone;
	
	@Length(max = 11)
	@NotNull
	@Column(name = "CELULAR", nullable = false, length = 11)
	protected String celular;
	
	@NotNull
	@Email
	@Length(max = 60)
	@Column(name = "EMAIL", nullable = false, length = 40)
	protected String email;
	
}