package br.com.vjrcreative.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.vjrcreative.enums.Uf;
import lombok.Data;

@Data
@Entity
@Table(name = "ENDERECO", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "ID_CLIENTE"))
public class Endereco implements java.io.Serializable {

	private static final long serialVersionUID = 7210338852514383155L;

	@NotNull
	@Length(max = 70)
	@Column(name = "LOCALIDADE", nullable = false, length = 70)
	protected String localidade;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ESTADO", nullable = false)
	protected Uf uf = Uf.DF;

	@Id
	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENTE", nullable = false)
	protected Cliente cliente;
	
	@Length(max = 8)
	@Column(name = "CEP", length = 8)
	protected String cep;

	@NotNull
	@Length(max = 110)
	@Column(name = "LOGRADOURO", nullable = false, length = 110)
	protected String logradouro;

	@Length(max = 70)
	@Column(name = "COMPLEMENTO", length = 70)
	protected String complemento;

	@Length(max = 45)
	@Column(name = "BAIRRO", length = 45)
	protected String bairro;

	@Length(max = 8)
	@Column(name = "NUMERO", length = 8)
	protected String numero;
	
}