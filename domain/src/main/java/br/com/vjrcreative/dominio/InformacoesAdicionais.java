package br.com.vjrcreative.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "informacoes_adicionais", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "ID_CLIENTE"))
public class InformacoesAdicionais implements Serializable {
	
	private static final long serialVersionUID = 3546328264406339336L;

	private static final String INFORMACOES_ADICIONAIS_SEQ = "INFORMACOES_ADICIONAIS_SEQ";

	@Id
	@NotNull
	@SequenceGenerator(name = INFORMACOES_ADICIONAIS_SEQ, sequenceName = INFORMACOES_ADICIONAIS_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = INFORMACOES_ADICIONAIS_SEQ)
	@Column(name = "ID", unique = true, nullable = false, precision = 28, scale = 2)
	protected Long id;
	
	/**
	 * Cliente que pode ter te indicado
	 */
	@OneToOne
	@JoinColumn(name = "ID_CLIENTE", nullable = false)
	protected Cliente cliente;

}