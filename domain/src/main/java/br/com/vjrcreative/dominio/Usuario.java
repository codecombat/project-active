package br.com.vjrcreative.dominio;

import br.com.vjrcreative.util.BaseEntity;
import lombok.Getter;
import lombok.Setter;

public class Usuario extends BaseEntity {

	private static final long serialVersionUID = -7513960340778991187L;

	@Getter
	@Setter
	protected String nome;
	
	@Getter
	@Setter
	protected String senha;

}
