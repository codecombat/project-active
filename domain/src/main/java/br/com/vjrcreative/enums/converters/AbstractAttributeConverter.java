package br.com.vjrcreative.enums.converters;

import java.util.Objects;
import javax.persistence.AttributeConverter;
import org.apache.commons.lang3.StringUtils;

/**
 * Classe que é utilizada pra criar conversores de enum para colunas do banco de dados
 * 
 * @param <T> 
 */
public abstract class AbstractAttributeConverter<T> implements AttributeConverter<String, T> {
	
	@Override
	public T convertToDatabaseColumn(String attribute) {
		if (StringUtils.isBlank(attribute)) {
			return null;
		}
		return null;
	}
	
	@Override
	public String convertToEntityAttribute(T dbData) {
		if (Objects.isNull(dbData)) {
			return null;
		}
		return null;
	}

}
