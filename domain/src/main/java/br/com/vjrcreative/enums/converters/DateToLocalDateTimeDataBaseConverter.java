package br.com.vjrcreative.enums.converters;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Hibernate converter to convert Date to LocalDateTime in {@code BaseEntity}
 *
 */
@Converter
public class DateToLocalDateTimeDataBaseConverter implements AttributeConverter<Date, LocalDateTime> {

	@Override
	public LocalDateTime convertToDatabaseColumn(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

	@Override
	public Date convertToEntityAttribute(LocalDateTime entityAttrValue) {
		if (Objects.nonNull(entityAttrValue)) {
			return Date.from(entityAttrValue.atZone(ZoneId.systemDefault()).toInstant());
		}
		return null;
	}
}
