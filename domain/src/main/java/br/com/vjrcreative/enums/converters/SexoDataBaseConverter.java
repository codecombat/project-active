package br.com.vjrcreative.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

import br.com.vjrcreative.enums.Sexo;

@Converter
public class SexoDataBaseConverter implements AttributeConverter<Sexo, String> {

	@Override
	public String convertToDatabaseColumn(Sexo sexo) {
		if (sexo == null) {
			return null;
		}
		return sexo.getCodigo();
	}

	@Override
	public Sexo convertToEntityAttribute(String codigo) {
		if (StringUtils.isNotBlank(codigo)) {
			return Sexo.get(codigo);
		}
		return null;
	}
}
