package br.com.vjrcreative.enums;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum Sexo {
	
	MASCULINO("M", "Masculino"), FEMININO("F", "Feminino");

	private static final List<Sexo> VALUE_LIST = Stream.of(values())
			.collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));

	private String codigo;
	private String descricao;

	private Sexo(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static List<Sexo> valuesAsList() {
        return VALUE_LIST;
    }

	public static Sexo get(String codigo) {
		return valuesAsList().stream().filter(s -> s.getCodigo().equals(codigo)).findFirst().orElse(null);
	}
}