package br.com.vjrcreative.enums;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum Uf {

	AC(1, "Acre", "AC"),
	AL(2, "Alagoas", "AL"),
	AM(3, "Amazonas", "AM"),
	AP(4, "Amapá", "AP"),
	BA(5, "Bahia", "BA"),
	CE(6, "Ceará", "CE"),
	DF(7, "Distrito Federal", "DF"),
	ES(8, "Espírito Santo", "ES"),
	GO(9, "Goiás", "GO"),
	MA(10, "Maranhão", "MA"),
	MG(11, "Minas Gerais", "MG"),
	MS(12, "Mato Grosso do Sul", "MS"),
	MT(13, "Mato Grosso", "MT"),
	PA(14, "Pará", "PA"),
	PB(15, "Paraíba", "PB"),
	PE(16, "Pernambuco", "PE"),
	PI(17, "Piauí", "PI"),
	PR(18, "Paraná", "PR"),
	RJ(19, "Rio de Janeiro", "RJ"),
	RN(20, "Rio Grande do Norte", "RN"),
	RO(21, "Rondônia", "RO"),
	RR(22, "Roraima", "RR"),
	RS(23, "Rio Grande do Sul", "RS"),
	SC(24, "Santa Catarina", "SC"),
	SE(25, "Sergipe", "SE"),
	SP(26, "São Paulo", "SP"),
	TO(27, "Tocantins", "TO");
	
	private static final List<Uf> VALUE_LIST = Stream.of(values())
			.collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
	
	private Integer codigo;
	private String descricao;
	private String sigla;

	Uf(Integer codigo, String descricao, String sigla) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.sigla = sigla;
		
	}
	
	public static List<Uf> valuesAsList() {
        return VALUE_LIST;
    }

	public static Uf get(String sigla) {
		return valuesAsList().stream().filter(uf -> uf.getSigla().equals(sigla)).findFirst().orElse(null);
	}
}