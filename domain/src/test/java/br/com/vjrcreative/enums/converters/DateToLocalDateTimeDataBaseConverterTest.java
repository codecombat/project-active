package br.com.vjrcreative.enums.converters;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class DateToLocalDateTimeDataBaseConverterTest {
	
	public void convertDateToLocalDateTimeEntityAttributeTest() {
		LocalDateTime localDateTime = new DateToLocalDateTimeDataBaseConverter().convertToDatabaseColumn(new Date());
		Assert.assertNotNull(localDateTime);
		Assert.assertEquals(localDateTime.getHour(), LocalDateTime.now().getHour());
		Assert.assertEquals(localDateTime.getDayOfMonth(), LocalDateTime.now().getDayOfMonth());
		Assert.assertEquals(localDateTime.getDayOfYear(), LocalDateTime.now().getDayOfYear());
		Assert.assertEquals(localDateTime.getYear(), LocalDateTime.now().getYear());
	}
	
	public void convertLocalDateTimeToDateEntityAttributeTest() {
		LocalDateTime now = LocalDateTime.now();
		Date date = new DateToLocalDateTimeDataBaseConverter().convertToEntityAttribute(now);
		Assert.assertNotNull(date);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Assert.assertEquals(cal.get(Calendar.MONTH), now.getMonthValue() - 1);
		Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), now.getDayOfMonth());
		Assert.assertEquals(cal.get(Calendar.HOUR), now.getHour());
		Assert.assertEquals(cal.get(Calendar.MINUTE), now.getMinute());
		Assert.assertEquals(cal.get(Calendar.SECOND), now.getSecond());
		
	}

}
